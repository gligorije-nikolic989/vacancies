package tfzr.vacancies.web.dto;

import java.util.Date;

import tfzr.vacancies.model.Vacancy;

public class VacancyDTO {
	private Long id;

	private String jobTitle;

	private String companyName;

	private String jobDescription;

	private Date creationDate;

	private Date expiringDate;

	private Integer logCount;

	public VacancyDTO() {
		super();
	}

	public VacancyDTO(Vacancy vacancy) {
		super();
		this.id = vacancy.getId();
		this.jobTitle = vacancy.getJobTitle();
		this.companyName = vacancy.getCompanyName();
		this.jobDescription = vacancy.getJobDescription();
		this.creationDate = vacancy.getCreationDate();
		this.expiringDate = vacancy.getExpiringDate();
		if (vacancy.getLogs() != null) {
			this.logCount = vacancy.getLogs().size();
		}
	}

	public VacancyDTO(Long id, String jobTitle, String companyName,
			String jobDescription, Date creationDate, Date expiringDate) {
		super();
		this.id = id;
		this.jobTitle = jobTitle;
		this.companyName = companyName;
		this.jobDescription = jobDescription;
		this.creationDate = creationDate;
		this.expiringDate = expiringDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getExpiringDate() {
		return expiringDate;
	}

	public void setExpiringDate(Date expiringDate) {
		this.expiringDate = expiringDate;
	}

	public Integer getLogCount() {
		return logCount;
	}

	public void setLogCount(Integer logCount) {
		this.logCount = logCount;
	}

}
