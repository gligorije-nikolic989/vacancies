package tfzr.vacancies.web.dto;

import java.util.Date;

import tfzr.vacancies.model.Log;

public class LogDTO {
	private Long id;
	private Date dateApplied;
	private VacancyDTO vacancy;
	private UserDTO user;

	public LogDTO() {

	}

	public LogDTO(Long id, Date dateApplied, VacancyDTO vacancy, UserDTO user) {
		super();
		this.id = id;
		this.dateApplied = dateApplied;
		this.vacancy = vacancy;
		this.user = user;
	}

	public LogDTO(Log log) {
		this.id = log.getId();
		this.dateApplied = log.getDateApplied();
		this.vacancy = new VacancyDTO(log.getVacancy());
		this.user = new UserDTO(log.getUser());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateApplied() {
		return dateApplied;
	}

	public void setDateApplied(Date dateApplied) {
		this.dateApplied = dateApplied;
	}

	public VacancyDTO getVacancy() {
		return vacancy;
	}

	public void setVacancy(VacancyDTO vacancy) {
		this.vacancy = vacancy;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

}