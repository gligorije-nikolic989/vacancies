package tfzr.vacancies.service;

import java.util.List;

import org.springframework.data.domain.Page;

import tfzr.vacancies.model.Vacancy;

public interface VacancyService {

	Vacancy findOne(Long id);

	Page<Vacancy> findAll(int page, int pageSize);

	Vacancy save(Vacancy vacancy);

	void remove(Long id) throws IllegalArgumentException;

	List<Vacancy> findByJobTitle(String string);

}
