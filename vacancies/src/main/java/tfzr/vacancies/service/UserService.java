package tfzr.vacancies.service;

import java.util.List;

import org.springframework.data.domain.Page;

import tfzr.vacancies.model.User;

public interface UserService {
	User findOne(Long id);

	Page<User> findAll(int page, int pageSize);

	User save(User user);

	void remove(Long id) throws IllegalArgumentException;

	List<User> findByEmail(String string);
}
