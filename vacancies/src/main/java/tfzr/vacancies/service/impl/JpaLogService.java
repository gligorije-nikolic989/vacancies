package tfzr.vacancies.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tfzr.vacancies.model.Log;
import tfzr.vacancies.repository.LogRepository;
import tfzr.vacancies.service.LogService;

@Transactional
@Service
public class JpaLogService implements LogService {

	@Autowired
	private LogRepository logRepository;

	@Override
	public Log save(Log log) {
		return logRepository.save(log);
	}

	@Override
	public ArrayList<Log> findAll() {
		return logRepository.findAll();
	}

}
