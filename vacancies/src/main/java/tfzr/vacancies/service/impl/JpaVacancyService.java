package tfzr.vacancies.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tfzr.vacancies.repository.VacancyRepository;
import tfzr.vacancies.model.Vacancy;
import tfzr.vacancies.service.VacancyService;

@Transactional
@Service
public class JpaVacancyService implements VacancyService {

	@Autowired
	private VacancyRepository vacancyRepository;

	@Override
	public Vacancy findOne(Long id) {

		return vacancyRepository.findOne(id);
	}

	@Override
	public Page<Vacancy> findAll(int page, int pageSize) {
		if (pageSize == -1) {
			pageSize = Integer.MAX_VALUE;
		}
		return vacancyRepository.findAll(new PageRequest(page, pageSize));
	}

	@Override
	public Vacancy save(Vacancy vacancy) {

		return vacancyRepository.save(vacancy);
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		Vacancy vacancy = vacancyRepository.findOne(id);
		if (vacancy == null) {
			throw new IllegalArgumentException(
					"Tried to remove nonexistant activity id:" + id);
		}

		vacancyRepository.delete(vacancy);
	}

	@Override
	public List<Vacancy> findByJobTitle(String string) {

		return vacancyRepository.findByJobTitleLike("%" + string + "%");
	}

}
