package tfzr.vacancies.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tfzr.vacancies.repository.UserRepository;
import tfzr.vacancies.model.User;
import tfzr.vacancies.service.UserService;

@Transactional
@Service
public class JpaUserService implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findOne(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public Page<User> findAll(int page, int pageSize) {
		if (pageSize == -1) {
			pageSize = Integer.MAX_VALUE;
		}
		return userRepository.findAll(new PageRequest(page, pageSize));
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		userRepository.delete(id);
	}

	// @Override
	// public Page<User> findByEmail(String string, int page) {
	//
	// return userRepository.findByEmailLike(("%" + string + "%"),
	// (new PageRequest(page, 5)));
	// }

	@Override
	public List<User> findByEmail(String string) {
		// TODO Auto-generated method stub
		return userRepository.findByEmailLike("%" + string + "%");
	}

}
