package tfzr.vacancies.service;

import java.util.ArrayList;

import tfzr.vacancies.model.Log;

public interface LogService {
	Log save(Log log);

	ArrayList<Log> findAll();

}
