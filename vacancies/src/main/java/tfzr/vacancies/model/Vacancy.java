package tfzr.vacancies.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tblVacancy")
public class Vacancy {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Long id;

	@Column(name = "jobTitle")
	private String jobTitle;

	@Column(name = "companyName")
	private String companyName;

	@Column(name = "jobDescription")
	private String jobDescription;

	@Column(name = "creationDate")
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@Column(name = "expiringDate")
	@Temporal(TemporalType.DATE)
	private Date expiringDate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "vacancy")
	private Set<Log> logs;

	private String adminComment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getExpiringDate() {
		return expiringDate;
	}

	public void setExpiringDate(Date expiringDate) {
		this.expiringDate = expiringDate;
	}

	public Set<Log> getLogs() {
		return logs;
	}

	public void setLogs(Set<Log> logs) {
		this.logs = logs;
	}

	public String getAdminComment() {
		return adminComment;
	}

	public void setAdminComment(String adminComment) {
		this.adminComment = adminComment;
	}

}
