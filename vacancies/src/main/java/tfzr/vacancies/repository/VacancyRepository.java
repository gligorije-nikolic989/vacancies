package tfzr.vacancies.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tfzr.vacancies.model.Vacancy;

@Repository
public interface VacancyRepository extends JpaRepository<Vacancy, Long> {

	Page<Vacancy> findAll(Pageable pageable);

	List<Vacancy> findByJobTitleLike(String jobTitle);

	//Page<Vacancy> findByOrderByCreationDateAsc(Pageable pageable);

}
