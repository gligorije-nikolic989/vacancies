package tfzr.vacancies.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tfzr.vacancies.model.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {
	ArrayList<Log> findAll();
}
