DROP DATABASE IF EXISTS vacancies;
CREATE DATABASE vacancies DEFAULT CHARACTER SET utf8;

USE vacancies;

GRANT ALL ON vacancies.* TO 'vacancies'@'%' IDENTIFIED BY 'vacancies';

FLUSH PRIVILEGES;