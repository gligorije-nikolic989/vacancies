vacanciesApp = angular.module('vacanciesApp', ['ngRoute'])

vacanciesApp.controller('VacancyController', function($scope, $http, $location, $routeParams) {
	
	$scope.page = 0;
	$scope.pageSize = 5;
	
	$scope.getVacancies = function() {
		
		var request_params = {};
		if ($scope.search) {
			request_params['jobTitle'] = $scope.search;
		}
		request_params['page'] = $scope.page;
		request_params['pageSize'] = $scope.pageSize;
		
		$http.get('api/vacancies', { params : request_params } )
				.success(function(data, status, headers) {
					$scope.vacancies = data;
					$scope.successMessage = 'Everything is ok.';
					
					$scope.totalPages = headers()['total-pages']
					$scope.totalElements = headers()['total-elements']
				})
				.error(function() {
					alert('Oops, something went wrong!');
				});
	};
	
	$scope.deleteVacancy = function(id, index) {
		$http.delete('api/vacancies/' + id)
			.success(function(){
					$scope.vacancies.splice(index, 1);
					$scope.succesMessage = 'Successfully removed.';
					$scope.errorMessage = false;
			})
			.error(function(){
				$scope.errorMessage = 'Error!';
				$scope.succesMessage = false;
			});
	};
	
	$scope.initVacancy = function() {
		$scope.vacancy = {};
		
		if ($routeParams && $routeParams.id) {
			$http.get('api/vacancies/' + $routeParams.id)
					.success(function(data) {
						$scope.vacancy = data;
					});
		}
	};
	
	$scope.saveVacancy = function() {
		if ($scope.vacancy.id) {
			$http.put('api/vacancies/'+ $scope.vacancy.id, $scope.vacancy)
					.success(function() {
						$location.path('/vacancies');
					});
		}
		else {
			$http.post('api/vacancies', $scope.vacancy)
					.success(function() {
						$location.path('/vacancies');
			});
		}
	};
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getVacancies();
	}
});

vacanciesApp.controller('UserController', function($scope, $http, $location, $routeParams) {
	
	$scope.page = 0;
	$scope.pageSize = 5;
	
	$scope.getUsers = function() { 
		var request_params = {};
		if ($scope.search) {
			request_params['email'] = $scope.search;
		}
		request_params['page'] = $scope.page;
		request_params['pageSize'] = $scope.pageSize;
		
		$http.get('api/users', { params : request_params } )
				.success(function(data, status, headers) {
					$scope.users = data;
					$scope.successMessage = 'Everything is ok.';
					
					$scope.totalPages = headers()['total-pages']
					$scope.totalElements = headers()['total-elements']
				})
				.error(function() {
					alert('Oops, something went wrong!');
				});
	};
	
	$scope.deleteUser = function(id, index) {
		$http.delete('api/users/' + id)
			.success(function(){
					$scope.users.splice(index, 1);
					$scope.succesMessage = 'Successfully removed.';
					$scope.errorMessage = false;
			})
			.error(function(){
				$scope.errorMessage = 'Error!';
				$scope.succesMessage = false;
			});
	};
	
	$scope.initUser = function() {
		$scope.user = {};
		
		if ($routeParams && $routeParams.id) {
			$http.get('api/users/' + $routeParams.id)
					.success(function(data) {
						$scope.user = data;
					});
		}
	};
	
	$scope.saveUser = function() {
		if ($scope.user.id) {
			$http.put('api/users/'+ $scope.user.id, $scope.user)
					.success(function() {
						$location.path('/users');
					});
		}
		else {
			$http.post('api/users', $scope.user)
					.success(function() {
						$location.path('/users');
			});
		}
	};
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getUsers();
	}
});

vacanciesApp.controller('LogController', function($scope, $http, $location, $routeParams) {
	
	$scope.initLog = function() {
		$scope.log = {};
		
		var request_params = {};
		request_params['page'] = 0;
		request_params['pageSize'] = -1;
		
		$http.get('api/vacancies', { params : request_params })
				.success(function(data) {
					$scope.vacancies = data;
				});
		$http.get('api/users', { params : request_params })
		.success(function(data) {
			$scope.users = data;
		});
	};
	
	$scope.saveLog = function() {
		$scope.log.vacancy = $scope.vacancySelected;
		$scope.log.user = $scope.userSelected;
		$http.post('api/logs', $scope.log)
				.success(function() {
					$location.path('/');
				});
	};
	
	$scope.getLogs = function() {
		if ($routeParams && $routeParams.vacancyId) {
			var request_params = {};
			
			request_params['vacancyId'] = $routeParams.vacancyId;
			request_params['userId'] = $routeParams.userId;
			
			$http.get('api/logs', { params : request_params})
					.success(function(data) {
						$scope.logs = data;
						$scope.successMessage = 'Everything is ok.';
					});
		}
		else if ($routeParams && $routeParams.userId) {
			var request_params = {};
			
			request_params['vacancyId'] = $routeParams.vacancyId;
			request_params['userId'] = $routeParams.userId;
			
			$http.get('api/logs', { params : request_params})
					.success(function(data) {
						$scope.logs = data;
						$scope.successMessage = 'Everything is ok.';
					});
		}else{
			var request_params = {};
			
			request_params['vacancyId'] = $routeParams.vacancyId;
			request_params['userId'] = $routeParams.userId;
			
			$http.get('api/logs', { params : request_params})
					.success(function(data) {
						$scope.logs = data;
						$scope.successMessage = 'Everything is ok.';
					});
		};
		
	};
	
	
});

vacanciesApp.filter('unique', function() {
	   return function(collection, keyname) {
	      var output = [], 
	          keys = [];

	      angular.forEach(collection, function(item) {
	          var key = item[keyname];
	          if(keys.indexOf(key) === -1) {
	              keys.push(key);
	              output.push(item);
	          }
	      });

	      return output;
	   };
	});



vacanciesApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'static/html/home.html'
        })
        .when('/vacancies', {
            templateUrl : 'static/html/vacancies.html',
            controller: 'VacancyController'
        })
        .when('/vacancies/add', {
            templateUrl : 'static/html/addEditVacancy.html',
            controller: 'VacancyController'
        })
        .when('/vacancies/edit/:id', {
            templateUrl : 'static/html/addEditVacancy.html',
            controller: 'VacancyController'
        })
        .when('/vacancies/:vacancyId/logs', {
            templateUrl : 'static/html/logsVacancies.html',
            controller: 'LogController'
        })
        
        .when('/users', {
            templateUrl : 'static/html/users.html',
            controller: 'UserController'
        })
        .when('/users/add', {
            templateUrl : 'static/html/addEditUser.html',
            controller: 'UserController'
        })
        .when('/users/edit/:id', {
            templateUrl : 'static/html/addEditUser.html',
            controller: 'UserController'
        })
         .when('/users/:userId/logs', {
            templateUrl : 'static/html/logsUsers.html',
            controller: 'LogController'
        })
        
        .when('/logs', {
            templateUrl : 'static/html/logs.html',
            controller: 'LogController'
        })
         .when('/logs/add', {
            templateUrl : 'static/html/addEditLog.html',
            controller: 'LogController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);